#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os,sys
import matplotlib.pyplot as plt
import array as arr
import numpy as np
import math
from mpl_toolkits import mplot3d
import pandas as pd
import csv
import glob
from scipy.stats import norm
import scipy.special as sp
#import plotly.graph_objs as go
from matplotlib.colors import LogNorm
import random 
from matplotlib.ticker import FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D

# importing Statistics module 
import statistics  

import time

start = time.time()



# In[2]:


# PARAMETERS
E=400  #GeV

alfa=45  #degrees
wmax = 900  #nm
wmin = 290 #nm
 #km
# Primary=1000000 #particles
# Cycles=97
# normed_value = 1/(Primary*Cycles)
R= 2.0  #radio fibra en cm
## Leer el archivo de datos y pasarlo a float. 
random.seed(a=None, version=2)
weight=5.98
#GeV
e=0.000511
p=0.938272
u=0.105660
pio=0.13957
kao=0.4937
n=0.939565

bth=0.685




# In[3]:


spill_particles=4e13


fileList2 = glob.glob('/eos/home-s/sbenitez/PhD/Data_Analysis/Fluka_results/LSS2/2020-09-25_2cm/LSS2_exp*_FIBER')                                     # 2 cm!!
Primary=100000 #particles
Cycles=10
weight2=5.98
normed_value2 = 1/(Primary*Cycles)*weight2*spill_particles


#print(fileList)
dfList = [] 
for ifile in fileList2:
    print(ifile)
    dfList += [ pd.read_csv(ifile,sep=r"\s+",skiprows=0) ]
df2 = pd.concat( dfList, ignore_index = True )
#print(df)


particles2 = list(dict.fromkeys(df2["PID"]))

df_Thr2= df2[df2['BETA'] > 0.685 ]

print ("Number of interactions", len(df2))
print ("Types of particles", particles2)

print ("Number of interactions beta", len(df_Thr2))
df_Thr2['ANGLE'] = np.arccos(df_Thr2["CZ"])*180/np.pi



fileList200 = glob.glob('/eos/home-s/sbenitez/PhD/Data_Analysis/Fluka_results/LSS2/2021-02-03_200micra_validation_full/LSS2_exp0*_FIBER')             # 200 micra!!
Primary200=1000000 #particles
Cycles200=97
weight200=5.98
normed_value200 = (1/(Primary200*Cycles200))*weight200*spill_particles


fileList100 = glob.glob('/eos/user/s/sbenitez/PhD/Data_Analysis/Fluka_results/LSS2/2021-02-17_100micra_validation_full/LSS2_exp0*_FIBER')        #100micra!!
Primary100=1000000 #particles
Cycles100=10
weight100=5.97
normed_value100 = (1/(Primary100*Cycles100))*weight100*spill_particles

fileList50 = glob.glob('/eos/user/s/sbenitez/PhD/Data_Analysis/Fluka_results/LSS2/2021-02-17_50micra_validation_full/LSS2_exp0*_FIBER')         #50 micra!!
Primary50=1000000 #particles
Cycles50=10
weight50=5.98
normed_value50 = (1/(Primary50*Cycles50))*weight50*spill_particles


#print(fileList)
dfList = [] 
for ifile in fileList200:
    print(ifile)
    dfList += [ pd.read_csv(ifile,sep=r"\s+",skiprows=0) ]
df200 = pd.concat( dfList, ignore_index = True )
#print(df)


particles200 = list(dict.fromkeys(df200["PID"]))



df_Thr200= df200[df200['BETA'] > 0.685 ]



print ("Number of interactions", len(df200))
print ("Types of particles", particles200)

print ("Number of interactions beta", len(df_Thr200))




#print(fileList)
dfList = [] 
for ifile in fileList100:
    print(ifile)
    dfList += [ pd.read_csv(ifile,sep=r"\s+",skiprows=0) ]
df100 = pd.concat( dfList, ignore_index = True )
#print(df)


particles100 = list(dict.fromkeys(df100["PID"]))



df_Thr100= df100[df100['BETA'] > 0.685 ]



print ("Number of interactions", len(df100))
print ("Types of particles", particles100)

print ("Number of interactions beta", len(df_Thr100))





#print(fileList)
dfList = [] 
for ifile in fileList50:
    print(ifile)
    dfList += [ pd.read_csv(ifile,sep=r"\s+",skiprows=0) ]
df50 = pd.concat( dfList, ignore_index = True )
#print(df)


particles50 = list(dict.fromkeys(df50["PID"]))



df_Thr50= df50[df50['BETA'] > 0.685 ]



print ("Number of interactions", len(df50))
print ("Types of particles", particles50)

print ("Number of interactions beta", len(df_Thr50))

print("hello")
end = time.time()
print(end - start)


# In[4]:


E0TRACK = []
NAMETRACK = []
CHARGETRACK = []
for index in df_Thr200['PID']:
    #print(index)
    if index == 1: 
        mass = p
        name = "Proton"
        
    elif index == 2:
        mass = p
        name = "Aproton"
        
        
    elif index == 3: 
        mass = e
        name = "Electron"
        
        
    elif index == 4: 
        mass = e
        name = "Positron"
        
       
    elif index == 7: 
        mass = 0
        name = "Photon"
        
       
    elif index == 8:
        mass = n
        name = "Neutron"
        
        
    elif index == 9:
        mass = n
        name = "Aneutron"
        
       
    elif index == 11: 
        mass = u
        name = "Muon-"
        
      
    elif index == 10: 
        mass = u
        name = "Muon+"
        
        
    elif index == 13: 
        mass = pio
        name = "Pion+"
        
        
    elif index == 14: 
        mass = pio
        name = "Pion-"
        
        
    elif index == 15: 
        mass = kao
        name = "Kaon+"
        
        
    elif index == 16: 
        mass = kao
        name = "Kaon-"
       
        
    elif index == 23: 
        mass = pio
        name = "Pion0"
        charge = 0
        
    elif index == 24: 
        mass = kao
        name = "Kaon0"
        
        
    elif index == 25: 
        mass = kao
        name = "AKaon0"

        
    elif index == -3: 
        mass = 1785612
        name = "$ Deuteron$"        
        
        
    elif index == -5: 
        mass = 0.015
        name = "$ ^{3}He$"
               
        
    E0TRACK += [mass]
    NAMETRACK +=[name]
    


df_Thr200["E0TRACK"] = E0TRACK
df_Thr200["NAMETRACK"] = NAMETRACK
NAMETRACK200 = NAMETRACK
# ANGLE (later the up and downstream)
df_Thr200['ANGLE'] = np.arccos(df_Thr200["CZ"])*180/np.pi



E0TRACK = []
NAMETRACK = []
CHARGETRACK = []
for index in df_Thr100['PID']:
    #print(index)
    if index == 1: 
        mass = p
        name = "Proton"
        
    elif index == 2:
        mass = p
        name = "Aproton"
        
        
    elif index == 3: 
        mass = e
        name = "Electron"
        
        
    elif index == 4: 
        mass = e
        name = "Positron"
        
       
    elif index == 7: 
        mass = 0
        name = "Photon"
        
       
    elif index == 8:
        mass = n
        name = "Neutron"
        
        
    elif index == 9:
        mass = n
        name = "Aneutron"
        
       
    elif index == 11: 
        mass = u
        name = "Muon-"
        
      
    elif index == 10: 
        mass = u
        name = "Muon+"
        
        
    elif index == 13: 
        mass = pio
        name = "Pion+"
        
        
    elif index == 14: 
        mass = pio
        name = "Pion-"
        
        
    elif index == 15: 
        mass = kao
        name = "Kaon+"
        
        
    elif index == 16: 
        mass = kao
        name = "Kaon-"
       
        
    elif index == 23: 
        mass = pio
        name = "Pion0"
        charge = 0
        
    elif index == 24: 
        mass = kao
        name = "Kaon0"
        
        
    elif index == 25: 
        mass = kao
        name = "AKaon0"

        
    elif index == -3: 
        mass = 1785612
        name = "$ Deuteron$"        
        
        
    elif index == -5: 
        mass = 0.015
        name = "$ ^{3}He$"
               
        
    E0TRACK += [mass]
    NAMETRACK +=[name]
    


df_Thr100["E0TRACK"] = E0TRACK
df_Thr100["NAMETRACK"] = NAMETRACK
NAMETRACK100 = NAMETRACK
# ANGLE (later the up and downstream)
df_Thr100['ANGLE'] = np.arccos(df_Thr100["CZ"])*180/np.pi




E0TRACK = []
NAMETRACK = []
CHARGETRACK = []
for index in df_Thr50['PID']:
    #print(index)
    if index == 1: 
        mass = p
        name = "Proton"
        
    elif index == 2:
        mass = p
        name = "Aproton"
        
        
    elif index == 3: 
        mass = e
        name = "Electron"
        
        
    elif index == 4: 
        mass = e
        name = "Positron"
        
       
    elif index == 7: 
        mass = 0
        name = "Photon"
        
       
    elif index == 8:
        mass = n
        name = "Neutron"
        
        
    elif index == 9:
        mass = n
        name = "Aneutron"
        
       
    elif index == 11: 
        mass = u
        name = "Muon-"
        
      
    elif index == 10: 
        mass = u
        name = "Muon+"
        
        
    elif index == 13: 
        mass = pio
        name = "Pion+"
        
        
    elif index == 14: 
        mass = pio
        name = "Pion-"
        
        
    elif index == 15: 
        mass = kao
        name = "Kaon+"
        
        
    elif index == 16: 
        mass = kao
        name = "Kaon-"
       
        
    elif index == 23: 
        mass = pio
        name = "Pion0"
        charge = 0
        
    elif index == 24: 
        mass = kao
        name = "Kaon0"
        
        
    elif index == 25: 
        mass = kao
        name = "AKaon0"

        
    elif index == -3: 
        mass = 1785612
        name = "$ Deuteron$"        
        
        
    elif index == -5: 
        mass = 0.015
        name = "$ ^{3}He$"
               
        
    E0TRACK += [mass]
    NAMETRACK +=[name]
    


df_Thr50["E0TRACK"] = E0TRACK
df_Thr50["NAMETRACK"] = NAMETRACK
NAMETRACK50 = NAMETRACK
# ANGLE (later the up and downstream)
df_Thr50['ANGLE'] = np.arccos(df_Thr50["CZ"])*180/np.pi


# In[5]:


wmax = 900  #nm
wmin = 290  #nm


# #2 cm
# cte = (2*np.pi/137)*(np.sin(47*(np.pi/180))*np.sin(47*(np.pi/180)))
# c=(0.83*1e11)
# w=np.arange(0,1001,1)

 
# df_Thr2["PHOTONS"]= cte*df_Thr2["CTRACK"]*1e7*((1/(wmin))-(1/(wmax)))

#df_Thr= df[df['BETA'] > 0.685 ]
# print ("Number of interactions", len(df))
# print ("Number of interactions beta", len(df_Thr))

#cte = (2*np.pi/137)*(np.sin(47*(np.pi/180))*np.sin(47*(np.pi/180)))



#200
cte = (2*np.pi/137)*(np.sin(47*(np.pi/180))*np.sin(47*(np.pi/180)))
c=(0.83*1e11)

 
df_Thr200["PHOTONS"]= cte*df_Thr200["CTRACK"]*1e7*((1/(wmin))-(1/(wmax)))


print (sum(df_Thr200["PHOTONS"]) , 'Photons generated')
#print (sum(df_Thr200["PHOTONS"])*normed_value, 'Photons generated/Primaries')

#weight200=5.98

Photons200 = sum(df_Thr200["PHOTONS"])*normed_value200

print (Photons200, 'Final photons normalized (one cycle)')
print("--------------------------------------------------------------------")

#100
cte = (2*np.pi/137)*(np.sin(47*(np.pi/180))*np.sin(47*(np.pi/180)))
c=(0.83*1e11)

 
df_Thr100["PHOTONS"]= cte*df_Thr100["CTRACK"]*1e7*((1/(wmin))-(1/(wmax)))


print (sum(df_Thr100["PHOTONS"]) , 'Photons generated')
#print (sum(df_Thr100["PHOTONS"])*normed_value, 'Photons generated/Primaries')

#weight100=5.97

Photons100 = sum(df_Thr100["PHOTONS"])*normed_value100

print (Photons100, 'Final photons normalized (one cycle)')


print("--------------------------------------------------------------------")
#50

cte = (2*np.pi/137)*(np.sin(47*(np.pi/180))*np.sin(47*(np.pi/180)))
c=(0.83*1e11)

 
df_Thr50["PHOTONS"]= cte*df_Thr50["CTRACK"]*1e7*((1/(wmin))-(1/(wmax)))


print (sum(df_Thr50["PHOTONS"]) , 'Photons generated')
#print (sum(df_Thr50["PHOTONS"])*normed_value, 'Photons generated/Primaries')

#weight50=5.98

Photons50 = sum(df_Thr50["PHOTONS"])*normed_value50

print (Photons50, 'Final photons normalized (one cycle)')


# In[6]:


# Upstream & Downstream


df_ThrDown2= df_Thr2[df_Thr2['ANGLE'] < 90 ]
df_ThrUp2= df_Thr2[df_Thr2['ANGLE'] > 90 ]
df_ThrDown2['ANGLE'][np.isnan(df_ThrDown2['ANGLE'])] = 0
df_ThrUp2['ANGLE'][np.isnan(df_ThrUp2['ANGLE'])] = 0


# PhotonsDown2 = sum(df_ThrDown2["PHOTONS"])
# PhotonsUp2= sum(df_ThrUp2["PHOTONS"])

# print (PhotonsDown2*normed_value2, 'Final photons Down')
# print (PhotonsUp2*normed_value2, 'Final photons Up')


print("------------------------------------------------")

df_ThrDown200= df_Thr200[df_Thr200['ANGLE'] < 90 ]
df_ThrUp200= df_Thr200[df_Thr200['ANGLE'] > 90 ]
df_ThrDown200['ANGLE'][np.isnan(df_ThrDown200['ANGLE'])] = 0
df_ThrUp200['ANGLE'][np.isnan(df_ThrUp200['ANGLE'])] = 0


PhotonsDown200 = sum(df_ThrDown200["PHOTONS"])
PhotonsUp200 = sum(df_ThrUp200["PHOTONS"])

print (PhotonsDown200*normed_value200, 'Final photons Down')
print (PhotonsUp200*normed_value200, 'Final photons Up')


print("------------------------------------------------")

df_ThrDown100= df_Thr100[df_Thr100['ANGLE'] < 90 ]
df_ThrUp100= df_Thr100[df_Thr100['ANGLE'] > 90 ]
df_ThrDown100['ANGLE'][np.isnan(df_ThrDown100['ANGLE'])] = 0
df_ThrUp100['ANGLE'][np.isnan(df_ThrUp100['ANGLE'])] = 0


PhotonsDown100 = sum(df_ThrDown100["PHOTONS"])
PhotonsUp100 = sum(df_ThrUp100["PHOTONS"])

print (PhotonsDown100*normed_value100, 'Final photons Down')
print (PhotonsUp100*normed_value100, 'Final photons Up')
print("------------------------------------------------")


df_ThrDown50= df_Thr50[df_Thr50['ANGLE'] < 90 ]
df_ThrUp50= df_Thr50[df_Thr50['ANGLE'] > 90 ]
df_ThrDown50['ANGLE'][np.isnan(df_ThrDown50['ANGLE'])] = 0
df_ThrUp50['ANGLE'][np.isnan(df_ThrUp50['ANGLE'])] = 0


PhotonsDown50 = sum(df_ThrDown50["PHOTONS"])
PhotonsUp50 = sum(df_ThrUp50["PHOTONS"])

print (PhotonsDown50*normed_value50, 'Final photons Down')
print (PhotonsUp50*normed_value50, 'Final photons Up')


# In[7]:


#200m fiber
df_ThrUp200['LFIBUPWITHNEG']=(2400+df_ThrUp200['ZN'])*0.00001     # 24 m desde 0 en FLUKA hasta delante de puerta amarilla
df_ThrDown200['LFIBDOWNWITHNEG']=(17600-df_ThrDown200['ZN'])*0.00001  # pasamos a km      hasta 200 m   **perdidas por intersección conectores no incluídas


df_ThrDown200['LFIBDOWN'] = [x if x > 0 else 0.0000001 for x in df_ThrDown200['LFIBDOWNWITHNEG']]  #si sale negativo, lo hago 0
df_ThrUp200['LFIBUP'] = [x if x > 0 else 0.0000001 for x in df_ThrUp200['LFIBUPWITHNEG']]


b_D=df_ThrDown200['BETA']
b_U=df_ThrUp200['BETA']
#b=1.0
NA=0.22
nco=1.46
# aux1= b*np.sqrt(nco*nco-NA*NA)
# aux2=np.sqrt(b*b*nco*nco-1)
aux1_D= b_D*np.sqrt(nco*nco-NA*NA)
aux2_D=np.sqrt(b_D*b_D*nco*nco-1)
aux1_U= b_U*np.sqrt(nco*nco-NA*NA)
aux2_U=np.sqrt(b_U*b_U*nco*nco-1)



# df_ThrDown['PROBABILITY']=(1/np.pi)*(np.arccos((aux1-np.cos((df_ThrDown['ANGLE'])*(np.pi/180)))/(np.sin((df_ThrDown['ANGLE'])*(np.pi/180))*aux2)))
# df_ThrUp['PROBABILITY']=(1/np.pi)*(np.arccos((aux1-np.cos((-90+df_ThrUp['ANGLE'])*(np.pi/180)))/(np.sin((-90+df_ThrUp['ANGLE'])*(np.pi/180))*aux2)))
# df_ThrDown['PROBABILITY'][np.isnan(df_ThrDown['PROBABILITY'])] = 0
# df_ThrUp['PROBABILITY'][np.isnan(df_ThrUp['PROBABILITY'])] = 0


df_ThrDown200['PROBABILITY']=(1/np.pi)*(np.arccos((aux1_D-np.cos((df_ThrDown200['ANGLE'])*(np.pi/180)))/(np.sin((df_ThrDown200['ANGLE'])*(np.pi/180))*aux2_D)))
df_ThrUp200['PROBABILITY']=(1/np.pi)*(np.arccos((aux1_U-np.cos((-90+df_ThrUp200['ANGLE'])*(np.pi/180)))/(np.sin((-90+df_ThrUp200['ANGLE'])*(np.pi/180))*aux2_U)))
df_ThrDown200['PROBABILITY'][np.isnan(df_ThrDown200['PROBABILITY'])] = 0
df_ThrUp200['PROBABILITY'][np.isnan(df_ThrUp200['PROBABILITY'])] = 0




# In[8]:


#200m fiber
df_ThrUp100['LFIBUPWITHNEG']=(2400+df_ThrUp100['ZN'])*0.00001     # 24 m desde 0 en FLUKA hasta delante de puerta amarilla
df_ThrDown100['LFIBDOWNWITHNEG']=(17600-df_ThrDown100['ZN'])*0.00001  # pasamos a km      hasta 200 m   **perdidas por intersección conectores no incluídas


df_ThrDown100['LFIBDOWN'] = [x if x > 0 else 0.0000001 for x in df_ThrDown100['LFIBDOWNWITHNEG']]  #si sale negativo, lo hago 0
df_ThrUp100['LFIBUP'] = [x if x > 0 else 0.0000001 for x in df_ThrUp100['LFIBUPWITHNEG']]


b_D=df_ThrDown100['BETA']
b_U=df_ThrUp100['BETA']
#b=1.0
NA=0.22
nco=1.46
# aux1= b*np.sqrt(nco*nco-NA*NA)
# aux2=np.sqrt(b*b*nco*nco-1)
aux1_D= b_D*np.sqrt(nco*nco-NA*NA)
aux2_D=np.sqrt(b_D*b_D*nco*nco-1)
aux1_U= b_U*np.sqrt(nco*nco-NA*NA)
aux2_U=np.sqrt(b_U*b_U*nco*nco-1)



# df_ThrDown['PROBABILITY']=(1/np.pi)*(np.arccos((aux1-np.cos((df_ThrDown['ANGLE'])*(np.pi/180)))/(np.sin((df_ThrDown['ANGLE'])*(np.pi/180))*aux2)))
# df_ThrUp['PROBABILITY']=(1/np.pi)*(np.arccos((aux1-np.cos((-90+df_ThrUp['ANGLE'])*(np.pi/180)))/(np.sin((-90+df_ThrUp['ANGLE'])*(np.pi/180))*aux2)))
# df_ThrDown['PROBABILITY'][np.isnan(df_ThrDown['PROBABILITY'])] = 0
# df_ThrUp['PROBABILITY'][np.isnan(df_ThrUp['PROBABILITY'])] = 0


df_ThrDown100['PROBABILITY']=(1/np.pi)*(np.arccos((aux1_D-np.cos((df_ThrDown100['ANGLE'])*(np.pi/180)))/(np.sin((df_ThrDown100['ANGLE'])*(np.pi/180))*aux2_D)))
df_ThrUp100['PROBABILITY']=(1/np.pi)*(np.arccos((aux1_U-np.cos((-90+df_ThrUp100['ANGLE'])*(np.pi/180)))/(np.sin((-90+df_ThrUp100['ANGLE'])*(np.pi/180))*aux2_U)))
df_ThrDown100['PROBABILITY'][np.isnan(df_ThrDown100['PROBABILITY'])] = 0
df_ThrUp100['PROBABILITY'][np.isnan(df_ThrUp100['PROBABILITY'])] = 0



# In[9]:


#200m fiber
df_ThrUp50['LFIBUPWITHNEG']=(2400+df_ThrUp50['ZN'])*0.00001     # 24 m desde 0 en FLUKA hasta delante de puerta amarilla
df_ThrDown50['LFIBDOWNWITHNEG']=(17600-df_ThrDown50['ZN'])*0.00001  # pasamos a km      hasta 200 m   **perdidas por intersección conectores no incluídas


df_ThrDown50['LFIBDOWN'] = [x if x > 0 else 0.0000001 for x in df_ThrDown50['LFIBDOWNWITHNEG']]  #si sale negativo, lo hago 0
df_ThrUp50['LFIBUP'] = [x if x > 0 else 0.0000001 for x in df_ThrUp50['LFIBUPWITHNEG']]


b_D=df_ThrDown50['BETA']
b_U=df_ThrUp50['BETA']
#b=1.0
NA=0.22
nco=1.46
# aux1= b*np.sqrt(nco*nco-NA*NA)
# aux2=np.sqrt(b*b*nco*nco-1)
aux1_D= b_D*np.sqrt(nco*nco-NA*NA)
aux2_D=np.sqrt(b_D*b_D*nco*nco-1)
aux1_U= b_U*np.sqrt(nco*nco-NA*NA)
aux2_U=np.sqrt(b_U*b_U*nco*nco-1)



# df_ThrDown['PROBABILITY']=(1/np.pi)*(np.arccos((aux1-np.cos((df_ThrDown['ANGLE'])*(np.pi/180)))/(np.sin((df_ThrDown['ANGLE'])*(np.pi/180))*aux2)))
# df_ThrUp['PROBABILITY']=(1/np.pi)*(np.arccos((aux1-np.cos((-90+df_ThrUp['ANGLE'])*(np.pi/180)))/(np.sin((-90+df_ThrUp['ANGLE'])*(np.pi/180))*aux2)))
# df_ThrDown['PROBABILITY'][np.isnan(df_ThrDown['PROBABILITY'])] = 0
# df_ThrUp['PROBABILITY'][np.isnan(df_ThrUp['PROBABILITY'])] = 0


df_ThrDown50['PROBABILITY']=(1/np.pi)*(np.arccos((aux1_D-np.cos((df_ThrDown50['ANGLE'])*(np.pi/180)))/(np.sin((df_ThrDown50['ANGLE'])*(np.pi/180))*aux2_D)))
df_ThrUp50['PROBABILITY']=(1/np.pi)*(np.arccos((aux1_U-np.cos((-90+df_ThrUp50['ANGLE'])*(np.pi/180)))/(np.sin((-90+df_ThrUp50['ANGLE'])*(np.pi/180))*aux2_U)))
df_ThrDown50['PROBABILITY'][np.isnan(df_ThrDown50['PROBABILITY'])] = 0
df_ThrUp50['PROBABILITY'][np.isnan(df_ThrUp50['PROBABILITY'])] = 0


# In[10]:


### CTE GAMMA INC (a,x) ---> x = 1/lambda^4

#ctech = 1e7*(2*np.pi/137)*(np.sin(47*(np.pi/180))*np.sin(47*(np.pi/180)))
ctech_D = 1e7*(2*np.pi/137)*(np.sin(df_ThrDown200['ANGLE']*(np.pi/180))*np.sin(df_ThrDown200['ANGLE']*(np.pi/180)))
ctech_U = 1e7*(2*np.pi/137)*(np.sin(df_ThrUp200['ANGLE']*(np.pi/180))*np.sin(df_ThrUp200['ANGLE']*(np.pi/180)))



df_ThrDown200['CTEGAMMADOWN']=(0.83*1e11)*df_ThrDown200['LFIBDOWN']*np.log(10)  #pasamos todo a nm para que las dos lambda (está elevado a la 4) estén iguales, todos los cabios de units del exponente se queda en la cte 
df_ThrUp200['CTEGAMMAUP']=(0.83*1e11)*df_ThrUp200['LFIBUP']*np.log(10)


df_ThrDown200['CATTDOWN']=ctech_D*sp.gamma(1/4)*(1/df_ThrDown200['CTEGAMMADOWN']**(1/4))/4/sp.gamma(1/4)
df_ThrUp200['CATTUP']=ctech_U*(1/df_ThrUp200['CTEGAMMAUP']**(1/4))/4/sp.gamma(1/4)

df_ThrDown200['GAMMADOWN']=df_ThrDown200['CTRACK']*df_ThrDown200['CATTDOWN']*(sp.gammaincc(1/4,df_ThrDown200['CTEGAMMADOWN']/wmax**4)-(sp.gammaincc(1/4,df_ThrDown200['CTEGAMMADOWN']/wmin**4)))
                                                                                                     
df_ThrUp200['GAMMAUP']=df_ThrUp200['CTRACK']*df_ThrUp200['CATTUP']*(sp.gammaincc(1/4,df_ThrUp200['CTEGAMMAUP']/wmax**4)-(sp.gammaincc(1/4,df_ThrUp200['CTEGAMMAUP']/wmin**4)))


attdown200=sum(df_ThrDown200['GAMMADOWN'])
attup200=sum(df_ThrUp200['GAMMAUP'])



print (attdown200*normed_value200, 'photons attenuated Down / Primary')
print (attup200*normed_value200, 'photons attenuated Up / Primary')


print ('Ratio Down',attdown200/PhotonsDown200)
print ('Ratio Up', attup200/PhotonsUp200)


# In[11]:


### CTE GAMMA INC (a,x) ---> x = 1/lambda^4

#ctech = 1e7*(2*np.pi/137)*(np.sin(47*(np.pi/180))*np.sin(47*(np.pi/180)))
ctech_D = 1e7*(2*np.pi/137)*(np.sin(df_ThrDown100['ANGLE']*(np.pi/180))*np.sin(df_ThrDown100['ANGLE']*(np.pi/180)))
ctech_U = 1e7*(2*np.pi/137)*(np.sin(df_ThrUp100['ANGLE']*(np.pi/180))*np.sin(df_ThrUp100['ANGLE']*(np.pi/180)))



df_ThrDown100['CTEGAMMADOWN']=(0.83*1e11)*df_ThrDown100['LFIBDOWN']*np.log(10)  #pasamos todo a nm para que las dos lambda (está elevado a la 4) estén iguales, todos los cabios de units del exponente se queda en la cte 
df_ThrUp100['CTEGAMMAUP']=(0.83*1e11)*df_ThrUp100['LFIBUP']*np.log(10)


df_ThrDown100['CATTDOWN']=ctech_D*sp.gamma(1/4)*(1/df_ThrDown100['CTEGAMMADOWN']**(1/4))/4/sp.gamma(1/4)
df_ThrUp100['CATTUP']=ctech_U*(1/df_ThrUp100['CTEGAMMAUP']**(1/4))/4/sp.gamma(1/4)

df_ThrDown100['GAMMADOWN']=df_ThrDown100['CTRACK']*df_ThrDown100['CATTDOWN']*(sp.gammaincc(1/4,df_ThrDown100['CTEGAMMADOWN']/wmax**4)-(sp.gammaincc(1/4,df_ThrDown100['CTEGAMMADOWN']/wmin**4)))
                                                                                                     
df_ThrUp100['GAMMAUP']=df_ThrUp100['CTRACK']*df_ThrUp100['CATTUP']*(sp.gammaincc(1/4,df_ThrUp100['CTEGAMMAUP']/wmax**4)-(sp.gammaincc(1/4,df_ThrUp100['CTEGAMMAUP']/wmin**4)))


attdown100=sum(df_ThrDown100['GAMMADOWN'])
attup100=sum(df_ThrUp100['GAMMAUP'])



print (attdown100*normed_value100, 'photons attenuated Down / Primary')
print (attup100*normed_value100, 'photons attenuated Up / Primary')


print ('Ratio Down',attdown100/PhotonsDown100)
print ('Ratio Up', attup100/PhotonsUp100)


# In[12]:


### CTE GAMMA INC (a,x) ---> x = 1/lambda^4

#ctech = 1e7*(2*np.pi/137)*(np.sin(47*(np.pi/180))*np.sin(47*(np.pi/180)))
ctech_D = 1e7*(2*np.pi/137)*(np.sin(df_ThrDown50['ANGLE']*(np.pi/180))*np.sin(df_ThrDown50['ANGLE']*(np.pi/180)))
ctech_U = 1e7*(2*np.pi/137)*(np.sin(df_ThrUp50['ANGLE']*(np.pi/180))*np.sin(df_ThrUp50['ANGLE']*(np.pi/180)))



df_ThrDown50['CTEGAMMADOWN']=(0.83*1e11)*df_ThrDown50['LFIBDOWN']*np.log(10)  #pasamos todo a nm para que las dos lambda (está elevado a la 4) estén iguales, todos los cabios de units del exponente se queda en la cte 
df_ThrUp50['CTEGAMMAUP']=(0.83*1e11)*df_ThrUp50['LFIBUP']*np.log(10)


df_ThrDown50['CATTDOWN']=ctech_D*sp.gamma(1/4)*(1/df_ThrDown50['CTEGAMMADOWN']**(1/4))/4/sp.gamma(1/4)
df_ThrUp50['CATTUP']=ctech_U*(1/df_ThrUp50['CTEGAMMAUP']**(1/4))/4/sp.gamma(1/4)

df_ThrDown50['GAMMADOWN']=df_ThrDown50['CTRACK']*df_ThrDown50['CATTDOWN']*(sp.gammaincc(1/4,df_ThrDown50['CTEGAMMADOWN']/wmax**4)-(sp.gammaincc(1/4,df_ThrDown50['CTEGAMMADOWN']/wmin**4)))
                                                                                                     
df_ThrUp50['GAMMAUP']=df_ThrUp50['CTRACK']*df_ThrUp50['CATTUP']*(sp.gammaincc(1/4,df_ThrUp50['CTEGAMMAUP']/wmax**4)-(sp.gammaincc(1/4,df_ThrUp50['CTEGAMMAUP']/wmin**4)))


attdown50=sum(df_ThrDown50['GAMMADOWN'])
attup50=sum(df_ThrUp50['GAMMAUP'])



print (attdown50*normed_value50, 'photons attenuated Down / Primary')
print (attup50*normed_value50, 'photons attenuated Up / Primary')


print ('Ratio Down',attdown50/PhotonsDown50)
print ('Ratio Up', attup50/PhotonsUp50)


# In[13]:


# PROBABILIDAAD


df_ThrDown200['NPD']= df_ThrDown200['GAMMADOWN'] * df_ThrDown200['PROBABILITY']
df_ThrUp200['NPU']=   df_ThrUp200['GAMMAUP'] * df_ThrUp200['PROBABILITY']


EmittedD200=sum(df_ThrDown200['NPD'])
EmittedU200=sum(df_ThrUp200['NPU'])


print (EmittedD200*normed_value200, 'photons attenuated with Probability Down / Primary')
print (EmittedU200*normed_value200, 'photons attenuated with Probability Up / Primary')


print ('Ratio Down',EmittedD200/PhotonsDown200)
print ('Ratio Up', EmittedU200/PhotonsUp200)


# In[14]:


df_ThrDown100['NPD']= df_ThrDown100['GAMMADOWN'] * df_ThrDown100['PROBABILITY']
df_ThrUp100['NPU']=   df_ThrUp100['GAMMAUP'] * df_ThrUp100['PROBABILITY']


EmittedD100=sum(df_ThrDown100['NPD'])
EmittedU100=sum(df_ThrUp100['NPU'])


print (EmittedD100*normed_value100, 'photons attenuated with Probability Down / Primary')
print (EmittedU100*normed_value100, 'photons attenuated with Probability Up / Primary')

print ('Ratio Down',EmittedD100/PhotonsDown100)
print ('Ratio Up', EmittedU100/PhotonsUp100)


# In[15]:


df_ThrDown50['NPD']= df_ThrDown50['GAMMADOWN'] * df_ThrDown50['PROBABILITY']
df_ThrUp50['NPU']=   df_ThrUp50['GAMMAUP'] * df_ThrUp50['PROBABILITY']


EmittedD50=sum(df_ThrDown50['NPD'])
EmittedU50=sum(df_ThrUp50['NPU'])


print (EmittedD50*normed_value50, 'photons attenuated with Probability Down / Primary')
print (EmittedU50*normed_value50, 'photons attenuated with Probability Up / Primary')

print ('Ratio Down',EmittedD50/PhotonsDown50)
print ('Ratio Up', EmittedU50/PhotonsUp50)


# In[16]:


normed_value200_b = (1/(Primary200*Cycles200))*weight200
print (EmittedD200*normed_value200_b*1e13/2e5, 'photons down *spill')
print (EmittedU200*normed_value200_b*1e13/2e5, 'photons up *spill')


# In[17]:


normed_value100_b = (1/(Primary100*Cycles100))*weight100
print (EmittedD100*normed_value100_b*1e13/2e5, 'photons down *spill')
print (EmittedU100*normed_value100_b*1e13/2e5, 'photons up *spill')


# In[18]:


normed_value50_b = (1/(Primary50*Cycles50))*weight50
print (EmittedD50*normed_value50_b*1e13/2e5, 'photons down *spill')
print (EmittedU50*normed_value50_b*1e13/2e5, 'photons up *spill')


# In[19]:


print (EmittedD200*normed_value200/2e5, 'photons down *spill/turn')
print (EmittedU200*normed_value200/2e5, 'photons up *spill/turn')


# In[20]:


print (EmittedD100*normed_value100/2e5, 'photons down *spill/turn')
print (EmittedU100*normed_value100/2e5, 'photons up *spill/turn')


# In[21]:


print (EmittedD50*normed_value50/2e5, 'photons down *spill/turn')
print (EmittedU50*normed_value50/2e5, 'photons up *spill/turn')


# In[22]:


wmin


# In[23]:


wmax


# In[ ]:





# In[24]:


print("hello")
end = time.time()
print(end - start)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




